package msir2144MV.tests.F03_Tests;

import msir2144MV.controller.AppController;
import msir2144MV.exception.NotAbleToCreateStatisticsException;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestGetStatistica_Invalid {

    private AppController controller;

    @Before
    public void setUp() throws Exception{
        controller= new AppController();
    }

    @Test
    public void test1() {
        boolean exceptionCaught = false;
        try {
            controller.getStatistica();
        }
        catch (NotAbleToCreateStatisticsException ex) {
            if (ex.getMessage().equals("Repository-ul nu contine nicio intrebare!")) {
                exceptionCaught = true;
            }
        }
        finally {
            if(exceptionCaught)
                assertTrue(true);
            else
                fail();
        }
    }
}
