package msir2144MV.tests.F03_Tests;

import msir2144MV.controller.AppController;
import msir2144MV.exception.DuplicateIntrebareException;
import msir2144MV.exception.InputValidationFailedException;
import msir2144MV.exception.NotAbleToCreateStatisticsException;
import msir2144MV.model.Intrebare;
import msir2144MV.model.Statistica;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestGetStatistica_Valid {

    private AppController controller;

    @Before
    public void setUp() throws Exception{
        controller= new AppController();
    }

    @Test
    public void test1() {
        Intrebare intrebare1 = null;
        Statistica statistica = null;
        try {
            intrebare1 = new Intrebare("Enunt1?", "1)v1", "2)v2", "3)v3", "3", "Domeniu1");
            controller.addNewIntrebare(intrebare1);
            statistica = controller.getStatistica();
        }
        catch (NotAbleToCreateStatisticsException ex) {
            if(ex.getMessage().equals("Repository-ul nu contine nicio intrebare!")) {
                fail();
            }
        } catch (DuplicateIntrebareException e) {
            if(e.getMessage().equals("Intrebarea deja exista!")) {
                fail();
            }
        } catch (InputValidationFailedException e) {
            fail();
        } finally {
            assertTrue(statistica.getIntrebariDomenii().containsKey(intrebare1.getDomeniu())
                    && statistica.getIntrebariDomenii().containsValue(1) );
        }
    }
}
