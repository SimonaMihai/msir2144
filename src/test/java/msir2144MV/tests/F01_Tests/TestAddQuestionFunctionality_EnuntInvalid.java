package msir2144MV.tests.F01_Tests;


import msir2144MV.controller.AppController;
import msir2144MV.exception.DuplicateIntrebareException;
import msir2144MV.exception.InputValidationFailedException;
import msir2144MV.model.Intrebare;
import msir2144MV.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestAddQuestionFunctionality_EnuntInvalid {
	AppController controller;
	
	@Before
	public void setUp() throws Exception {
		controller = new AppController();
	}

	@Test
	public void test() {
		boolean exceptionCaught = false;
		String enunt = "";
		String varianta1 = "1)var1";
		String varianta2 = "2)var2";
		String varianta3 = "3)var3";
		String variantaCorecta="1";
		String domeniu = "Istorie";
		
		Intrebare intrebare;
		try{
			intrebare = new Intrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
			controller.addNewIntrebare(intrebare);
		}
		catch (InputValidationFailedException e){
			if(e.getMessage().equals("Enuntul este vid!")) {
				exceptionCaught = true;
			}
			System.out.println(e);
		}
		catch(DuplicateIntrebareException e){
			exceptionCaught=true;
		}
		finally{
			if(exceptionCaught)
				assertTrue(true);
			else
				fail();
		}
	}
}
